A pasta sysvol armazena scripts de logon/logoff, diretivas de grupos, etc. 

Instalação de pacotes no dc1 e dc2:

    apt-get install rsync

Procurar servidor master para ser o PDC emulator (tanto faz rodar de dc1 ou dc2):

    samba-tool fsmo show | grep PdcEmulationMasterRole
   
Supondo que o dc1 seja o master (PDC emulator), instalar o osync:

    wget https://github.com/deajan/osync/archive/v1.1.3.tar.gz
    tar -vzxf v1.1.3.tar.gz
    cd osync-1.1.3
    ./install.sh

Gerar chave no dc1 e enviar para o dc2:

    ssh-keygen -t dsa
    ssh-copy-id -i ~/.ssh/id_dsa.pub root@dc2

Gerar chave no dc2 e enviar para o dc1:

    ssh-keygen -t dsa
    ssh-copy-id -i ~/.ssh/id_dsa.pub root@dc1

Desligar samba no dc2, copiar arquivo idmap.ldb do dc1, depois ligar samba no dc2:

    tdbbackup -s bak /var/lib/samba/private/idmap.ldb
    scp /var/lib/samba/private/idmap.ldbbak root@dc2:/var/lib/samba/private/idmap.ldb

No master (dc1), configurar sincronismo:
    
    cd /etc/osync
    wget https://git.uspdigital.usp.br/5385361/archive/raw/master/cursos/astreinamentos.com.br/samba4/files/sync.conf
    /usr/local/bin/osync.sh /etc/osync/sync.conf
    cat /var/log/osync_sysvol.log

No dc2, reconstruir as permissões da pasta sysvol:

    samba-tool ntacl sysvolreset

Colocar no cron do dc1 (crontab -e):
    
    */5 * * * * root /usr/local/bin/osync.sh /etc/osync/sync.conf --silent

Para testar, pode criar um arquivo no dc1 e ver se aparece no dc2:

    touch /var/lib/samba/sysvol/pandora.fflch.usp.br/teste.txt
