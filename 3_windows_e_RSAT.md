Colocar windows no domínio:

 - Apontar o DNS primário para o IP do samba
 - No prompt de comando: ipconfig /flushdns
 - Sincronizar horário do windows com o do servidor (em horário da internet: a.st1.ntp.br)
 - No prompt de comando: sysdm.cpl para inserir máquina no domínio: pandora.fflch.usp.br
 - Usuário Administrator e senha criada no provisionamento
 - (Extra) Coloca no domínio e converter usuário local para usuário do domínio: 
   - https://www.forensit.com/Downloads/Profwiz3.zip

Links para RSAT: 

 - Windows 10: https://www.microsoft.com/pt-BR/download/details.aspx?id=45520
 - Windows 8: https://www.microsoft.com/pt-br/download/details.aspx?id=28972
 - Windows 8.1: https://www.microsoft.com/pt-br/download/details.aspx?id=39296
 - Windows 7 SP 1: https://www.microsoft.com/pt-br/download/details.aspx?id=7887

Depois de instalado, ativar RSAT:

 - Painel de Controle
 - Programas e Recursos -> Ativar e Desativar recursos do Windows
 - Ferramentas de Administração de Servidor Remoto
 - Em Ferramentas de Administração de Funções, deixar marcado apenas:
   - Ferramentas de Servidor DNS
   - Ferramentas de AD DS e do AD LDS (todas opções)
 - Em Ferramentas de Administração de Recursos, deixar marcado apenas: 
   -  Ferramentas de Gerenciamento de Diretiva de Grupo

Agora existe a opção "Usuários e Computadores do Active Directory" em 
Ferramentas Administrativa. Se clicar em PANDORA.FFLCH.USP.BR e em 
"Alterar nível funcional" é possível ver o nível funcional atual. 
E em "Domínio e relação de confiança do Active Directory" também 
é possível ver esse nível para a Floresta.


