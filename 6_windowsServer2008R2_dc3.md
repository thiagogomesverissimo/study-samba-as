1. Verificar se o nível funcional do samba4 está como "Windows Server 2008 R2".

2. Configurações no window Server 2008: 

 - Configurar timezone e atualizar horário (a.st1.ntp.br)
 - Hostname: dc3
 - ip: 192.168.100.13, mask: 255.255.255.0 
 - Apontar DNS primário para o 192.168.100.11
 - DNS secundário: 192.168.100.13 

3. Promover Windows Server para DC adicional, no prompt: dcpromo

 - usar modo avançado
 - Domínio existente
 - domain name: pandora.fflch.usp.br e usuário administrator 
 - Marcar: DNS server/Global Cataloging
 - Marcar: No update the DNS delegation
 - Marcar: Replicate data over network from an existing DC
 - Marcar: Let the wizard choose an appropiate DC
 - Depois de reiniciar, especificar o domínio junto com o usuário no login: pandora\administrator

5. Editar registro para sincronizar pasta sysvol.

    regedit
    Computer -> HKEY_LOCAL_MACHINE -> SYSTEM -> CurrentControlSet -> services -> Netlogon -> Parameters -> SysvolReady
    mudar ValueData de 0 para 1. 

6. Mapear a pasta sysvol do dc1 para S: 

    - \\dc1
    - Lado direito do mouse. Mapear e marcar reconectar no logon.

7. Executar script abaixo, sysvol.bat, que sincroniza a pasta sysvol dc1 (S:) 
com a pasta \\127.0.0.1\sysvol (ou C:\Windows\SYSVOL\sysvol) local:
 
    :1
    ROBOCOPY S: \\127.0.0.1\sysvol /MIR /MT:10 /Z /R:2 /W:2 /COPYALL
    goto 1 
    
    # Explicação dos parâmetros:
    # MT: 10  = arquivos por vez
    # R: 2    = duas tentativas
    # W: 2    = a cada 2 segundos 

8. Transformar sysvol.bat em binário, baixar e rodar http://www.f2ko.de/en/b2e.php, 
marcar a opção "Aplicação invisível".

9. Colocar sysvol.exe no boot:

    - Task Schedular
    - Em Task Schedular Library -> new Task
    - Marcar para rodar estando ou não o usuário conectado
    - Disparador: ao iniciar
    - Actions -> New -> Start a Program

10. Verrificar o estado da replicação no dc1:

    samba-tool drs showrepl
