ip: 192.168.100.21
hostname: dm1

Instalação via apt dos pacotes necessários:

    apt-get update && apt-get -y install samba winbind acl attr ntpdate ldb-tools krb5-user krb5-config libpam-winbind libnss-winbind smbclient

Configurar timezone:

    dpkg-reconfigure tzdata # "America/Sao_Paulo"

BUG no ubuntu 18.04, corrigir:

    systemctl disable smbd nmbd winbind
    systemctl mask smbd nmbd winbind
    systemctl stop smbd nmbd winbind

    systemctl enable samba-ad-dc
    systemctl unmask samba-ad-dc
    systemctl start samba-ad-dc

Procurar uma solução melhor, mas por ora desabilitar systemd para dns:

    sudo systemctl disable systemd-resolved.service
    sudo service systemd-resolved stop

Apagar link e recriar resolv.conf:

    rm /etc/resolv.conf
    touch /etc/resolv.conf

Em /etc/resolv.conf:

    nameserver 192.168.100.11
    nameserver 192.168.100.12
    search pandora.fflch.usp.br
    domain pandora.fflch.usp.br

Verificar se resolve dc1 e dc2:

    host -t A dc1.pandora.fflch.usp.br
    host -t A dc2.pandora.fflch.usp.br

Apontar a consulta ntp para nosso dc1, em /etc/ntp.conf:

    # local
    server 127.127.1.0
    fudge  127.127.1.0 stratum 10

    # nosso ntp server
    server pandora.fflch.usp.br iburst prefer

    driftfile /var/lib/ntp/ntp.drift
    logfile   /var/log/ntp

    # Controle de acesso
    restrict default ignore

    # controle
    restrict 127.0.0.1
    restrict pandora.fflch.usp.br mask 255.255.255.255 nomodify notrap nopeer noquery

Restart ntp:

    /etc/init.d/ntp restart
#    ntpdate -u a.st1.ntp.br

Em /etc/hosts inserir:

    192.168.100.21  dm1.pandora.fflch.usp.br dm1

Verificar se o DNS do dc1 será capaz de inserir o hostname dessa máquina como
domínio verificando:

    getent hosts dm1 # output dever ser: 192.168.100.21  dm1.pandora.fflch.usp.br dm1
    hostname         # output dever ser: dm1 
    hostname -f      # output dever ser: dm1.pandora.fflch.usp.br

Copia smb.conf para o domain member:
rm /etc/samba/smb.conf
    cd /etc/samba
    wget https://git.uspdigital.usp.br/5385361/archive/raw/master/cursos/astreinamentos.com.br/samba4/files/smb_client.conf -O smb.conf

Em /etc/krb5.conf deixar apenas (dúvidas em [realms]):

    [libdefaults]
	    default_realm = PANDORA.FFLCH.USP.BR
	    dns_lookup_realm = false
	    dns_lookup_kdc = false
	    ticket_lifetime = 24h
	    renew_lifetime = 7d
	    forwardable = true

    [realms]
	    PANDORA.FFLCH.USP.BR = {
	      kdc = DC1.PANDORA.FFLCH.USP.BR
	      admin_server = DC1.PANDORA.FFLCH.USP.BR
      }

Ingressar máquina dm1 no domínio:

    net ads join -U administrator

Editar /etc/nsswitch.conf:

    passwd:         compat winbind
    group:          compat winbind
    shadow:         compat windind

Restart winbind:

    /etc/init.d/winbind restart

Verificar se o winbind consegue listar os usuários e grupos do domínio:

    wbinfo -u
    wbinfo -g

Verificar se é possível fazer o mapeamento local dos usuários do
domínio:

    getent passwd
    getent group

Reiniciar a máquina e tentar logar como usuário do domínio
