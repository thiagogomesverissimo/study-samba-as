Informações sobre DNS server:

    samba-tool dns serverinfo 127.0.0.1 -U Administrator

Listar zonas:

    samba-tool dns zonelist 127.0.0.1 -U Administrator

Listar zonas reversas:

    samba-tool dns zonelist 127.0.0.1 --reverse -U Administrator

Informações de uma zona em especial:

    samba-tool dns zoneinfo 127.0.0.1 pandora.fflch.usp.br -U Administrator

Lista entradas em numa zona:

    samba-tool dns query 127.0.0.1 pandora.fflch.usp.br @ ALL -U Administrator

Lista entradas em numa zona reversa:

    samba-tool dns query 127.0.0.1 100.168.192.in-addr.arpa @ ALL -U Administrator

Criar zona (reversa):
    
    samba-tool dns zonecreate 127.0.0.1 100.168.192.in-addr.arpa -U Administrator

Deletar zona:

    samba-tool dns zonedelete  127.0.0.1 100.168.192.in-addr.arpa -U Administrator

Adicionar registro A para máquina windows10 apondando para o ip 192.168.100.55: 

    samba-tool dns add 127.0.0.1 pandora.fflch.usp.br windows10 A 192.168.100.55 -U Administrator

Adicionar registro do tipo CNAME apontando win10 para windows10:

    samba-tool dns add 127.0.0.1 pandora.fflch.usp.br win10 cname windows10 -U Administrator

Criar um registro PTR na zona reversa, apontando 192.168.100.55 para windows10.pandora.fflch.usp.br:

    samba-tool dns add 127.0.0.1 100.168.192.in-addr.arpa 55 PTR windows10.pandora.fflch.usp.br -U Administrator

Atualizando registro tipo A, trocar ip de windows10 de final 55 para 56:

    samba-tool dns update 127.0.0.1 pandora.fflch.usp.br windows10 A 192.168.100.55 192.168.100.56 -U Administrator
