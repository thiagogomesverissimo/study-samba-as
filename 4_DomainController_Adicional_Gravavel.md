ip: 192.168.100.12
hostname: dc2

Links:
 
 - https://wiki.samba.org/index.php/Setting_up_Samba_as_a_Domain_Member
 - https://wiki.samba.org/index.php/Verifying_and_Creating_a_DC_DNS_Record
 - mhttps://wiki.samba.org/index.php/Demoting_a_Samba_AD_DC

Instalação de pacotes:

    apt-get update && apt-get -y install samba winbind acl attr ntpdate ldb-tools krb5-user krb5-config libpam-winbind libnss-winbind smbclient

BUG no ubuntu 18.04, corrigir:

    systemctl disable smbd nmbd winbind
    systemctl mask smbd nmbd winbind
    systemctl stop smbd nmbd winbind

    systemctl enable samba-ad-dc
    systemctl unmask samba-ad-dc
    systemctl start samba-ad-dc

No /etc/fstab, inserir essas opções na na raiz(/):
    
    errors=remount-ro,acl,user_xattr,barrier=1

Remontar sem precisar reiniciar:
 
    mount -o remount /

Atualiza a hora:
    
    dpkg-reconfigure tzdata # "America/Sao_Paulo"
    ntpdate -u a.st1.ntp.br

Procurar uma solução melhor, mas por ora desabilitar systemd para dns:

    sudo systemctl disable systemd-resolved.service
    sudo service systemd-resolved stop

Apagar link e recriar resolv.conf:

    rm /etc/resolv.conf
    touch /etc/resolv.conf

Em /etc/resolv.conf:

    nameserver 192.168.100.11
    nameserver 192.168.100.12
    search pandora.fflch.usp.br
    domain pandora.fflch.usp.br

Verificar se a resolução do DNS do samba está funcionado normamente:

    nslookup dc1.pandora.fflch.usp.br
    
    # Resposta:
    Server:		192.168.100.11
    Address:	192.168.100.11#53

    Name:	dc1.pandora.fflch.usp.br
    Address: 192.168.100.11

    nslookup pandora.fflch.usp.br
    
    # Resposta:
    Server:		192.168.100.11
    Address:	192.168.100.11#53

    Name:	pandora.fflch.usp.br
    Address: 192.168.100.11

Ver se dc2 resolve dc1:

    host -t A dc1.pandora.fflch.usp.br

Em /etc/krb5.conf deixar apenas:

    [libdefaults]
        dns_lookup_realm = false
        dns_lookup_kdc = true
        default_realm = pandora.fflch.usp.br

    [realms]
    PANDORA.FFLCH.USP.BR = {
        kdc = 192.168.100.11
        admin_server = 192.168.100.11
    }

Gerar ticket:

    kinit administrator@PANDORA.FFLCH.USP.BR
    klist

Provisionar dc2 como dc adicional:

    rm /etc/samba/smb.conf
    samba-tool domain join PANDORA.FFLCH.USP.BR DC -U administrator --realm=PANDORA.FFLCH.USP.BR --dns-backend=SAMBA_INTERNAL 

Para conferência, no DC1 verificar se o DC2 foi inserido no DNS:

    samba-tool dns query localhost pandora.fflch.usp.br @ ALL -U administrator

Se sim, a resposta de *nslookup dc2.pandora.fflch.usp.br* no dc1 deve ser:

    Server:		192.168.100.11
    Address:	192.168.100.11#53

    Name:	dc2.pandora.fflch.usp.br
    Address: 192.168.100.12

Verificar os Domain Controllers ativis na florestas:

    ldbsearch -H /var/lib/samba/private/sam.ldb '(invocationId=*)' --cross-ncs objectguid

Reboot dc1 e dc2. Pelo RSAT em "Usuários e Computadores do Active Directory", 
com o lado direito do mouse, Alterar Controlador de Domínio e verificar se dc1 e 
dc2 estão disponíveis. 
Criar usuário no DC1 e  testar login no DC2 e vice-versa:

    samba-tool user create user1 # DC1
    smbclient -L localhost -U user1 # DC2

    samba-tool user create user2 # DC2
    smbclient -L localhost -U user2 # DC1

Verificar estado da replicação em DC1 e DC2:

    samba-tool drs showrepl

Pra conferência, verificar replicação de ambos lados, no DC1:

    samba-tool drs replicate DC2 DC1 dc=pandora,dc=fflch,dc=usp,dc=br
    samba-tool drs replicate DC2 DC1 DC=ForestDnsZones,dc=pandora,dc=fflch,dc=usp,dc=br
    samba-tool drs replicate DC2 DC1 CN=Configuration,dc=pandora,dc=fflch,dc=usp,dc=br
    samba-tool drs replicate DC2 DC1 DC=DomainDnsZones,dc=pandora,dc=fflch,dc=usp,dc=br
    samba-tool drs replicate DC2 DC1 CN=Schema,CN=Configuration,dc=pandora,dc=fflch,dc=usp,dc=br

No DC2:

    samba-tool drs replicate DC1 DC2 dc=pandora,dc=fflch,dc=usp,dc=br
    samba-tool drs replicate DC1 DC2 DC=ForestDnsZones,dc=pandora,dc=fflch,dc=usp,dc=br
    samba-tool drs replicate DC1 DC2 CN=Configuration,dc=pandora,dc=fflch,dc=usp,dc=br
    samba-tool drs replicate DC1 DC2 DC=DomainDnsZones,dc=pandora,dc=fflch,dc=usp,dc=br
    samba-tool drs replicate DC1 DC2 CN=Schema,CN=Configuration,dc=pandora,dc=fflch,dc=usp,dc=br
