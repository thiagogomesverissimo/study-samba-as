ip: 192.168.100.11
hostname: dc1
Domínio apontado para esse ip: pandora.fflch.usp.br

Instalação via apt dos pacotes necessários:

    apt-get update && apt-get -y install samba winbind acl attr ntpdate ldb-tools krb5-user krb5-config libpam-winbind libnss-winbind smbclient

BUG no ubuntu 18.04, corrigir:

    systemctl disable smbd nmbd winbind
    systemctl mask smbd nmbd winbind
    systemctl stop smbd nmbd winbind

    systemctl enable samba-ad-dc
    systemctl unmask samba-ad-dc
    systemctl start samba-ad-dc

Atualiza a hora:

    dpkg-reconfigure tzdata # "America/Sao_Paulo"
    ntpdate -u a.st1.ntp.br

Procurar uma solução melhor, por ora desabilitar systemd para dns:

    sudo systemctl disable systemd-resolved.service
    sudo service systemd-resolved stop

Apagar link e recriar resolv.conf:

    rm /etc/resolv.conf
    touch /etc/resolv.conf

Em /etc/resolv.conf:

    nameserver 192.168.100.11
    nameserver 192.168.100.12
    search pandora.fflch.usp.br
    domain pandora.fflch.usp.br

No /etc/fstab, inserir essas opções na na raiz(/):

    errors=remount-ro,acl,user_xattr,barrier=1

Remontar sem precisar reiniciar:

    mount -o remount /

Provisionar o samba, sendo eth1 a interface que está na mesma rede dos clientes:

    rm /etc/samba/smb.conf
    samba-tool domain provision --use-rfc2307 --interactive
    Realm: PANDORA.FFLCH.USP.BR
    Domain: PANDORA
    Server Role: dc
    DNS backend: SAMBA_INTERNAL
    DNS Forwarder: 8.8.8.8

Copiar o arquivo kerberos gerado para etc:

    cp /var/lib/samba/private/krb5.conf /etc/

Se o servidor tiver mais que uma interface, na seção global do smb.conf,
obrigatóriamente definir em qual interface o samba irá operar:

    interfaces = eth1 lo
    bind interfaces only = yes

É uma boa rodar o testparm para verificar as opções default:

    testparm -v

Restart serviço do samba:

    systemctl start samba-ad-dc
    systemctl start samba-ad-dc
    systemctl start samba-ad-dc

Para verificar se o servidor está ouvindo nas portas do samba:

    netstat -pultan

Criar usuário pela linha de comando:

    samba-tool user create thiago
    samba-tool user list

Testar conexão:

    smbclient -L localhost -U thiago

Verificar se a resolução do DNS do samba está funcionado normamente:

    nslookup dc1.pandora.fflch.usp.br
    
    # Resposta:
    Server:		192.168.100.11
    Address:	192.168.100.11#53

    Name:	dc1.pandora.fflch.usp.br
    Address: 192.168.100.11

    nslookup pandora.fflch.usp.br
    
    # Resposta:
    Server:		192.168.100.11
    Address:	192.168.100.11#53

    Name:	pandora.fflch.usp.br
    Address: 192.168.100.11

Também verificar a resposta de *systemd-resolve --status*:

    DNS Servers: 192.168.100.11
    DNS Domain: pandora.fflch.usp.br
    DNSSEC NTA: 10.in-addr.arpa
    ...




