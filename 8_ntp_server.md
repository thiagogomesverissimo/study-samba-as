# Transformar o dc1 em ntp server

Instala ntp:

    apt-get install ntp

AJusta permissões:

    chown root:ntp /var/lib/samba/ntp_signd
    chmod 750 /var/lib/samba/ntp_signd

Configuração do /etc/ntp.conf:

    # local
    server 127.127.1.0
    fudge  127.127.1.0 stratum 10

    # nic br
    server a.st1.ntp.br iburst prefer

    # log
    driftfile       /var/lib/ntp/ntp.drift
    logfile         /var/log/ntp
    ntpsigndsocket  /usr/local/samba/var/lib/ntp_signd/

    # Controle de acesso
    restrict default kod nomodify notrap nopeer mssntp
    restrict 127.0.0.1
    restrict 0.pool.ntp.org   mask 255.255.255.255    nomodify notrap nopeer noquery

Restart no serviço ntp:

    /etc/init.d/ntp restart

No windows client:

 - Ferramentas administrativas
 - Gerenciamento de política de grupo
 - pandora.fflch.usp.br -> Default domain policy -> editar
 - Configurações do computador
 - Políticas
 - Modelos adminstrativos
 - Sistema
 - Serviço de tempo do windows 
 - provedores de tempo
 - Configurar window ntp client: habilitar
 - ntpserver: dc1.pandora.fflch.usp.br

No prompt do windows:

    gpupdate /force
    gpresult /h gpo.html
