Instalação das dependências (https://wiki.samba.org/index.php/Installing_Samba):

    apt-get install libreadline-dev git build-essential libattr1-dev libblkid-dev   \ 
                    autoconf python-dev python-dnspython libacl1-dev gdb pkg-config \
                    libpopt-dev libldap2-dev dnsutils acl attr libbsd-dev docbook-xsl \
                    libcups2-dev libgnutls28-dev

Baixando o samba e compilando samba:

    wget https://download.samba.org/pub/samba/stable/samba-4.5.0.tar.gz
    ./configure --prefix=/opt/samba
    make
    make install

Configurando o path para o usuário root:

    echo 'export PATH=$PATH:/opt/samba/bin' >> /root/.bashrc
    echo 'export PATH=$PATH:/opt/samba/sbin' >> /root/.bashrc

Colocando para iniciar no boot do SO:

    # criar arquivo: /lib/systemd/system/samba.service
    [Unit]
    Description=Samba AD Daemon
    After=syslog.target network.target
    
    [Service]
    Type=forking
    NotifyAccess=all
    PIDFile=/opt/samba/var/run/samba.pid
    LimitNOFILE=16384
    EnvironmentFile=-/etc/sysconfig/samba
    ExecStart=/opt/samba/sbin/samba -D
    ExecReload=/bin/kill -HUP $MAINPID
    ExecStop=/usr/bin/killall samba
    Restart=on-failure

    [Install]
    WantedBy=multi-user.target

Testando os comandos gerados:
 
    systemctl enable samba.service
    systemctl start samba
