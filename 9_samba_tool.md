Alguns comandos úteis:

    samba-tool user add thiago1 "Adm123&*"
    samba-tool user setpassword thiago1 --newpassword="Adm@#87Rtak09"
    samba-tool user setpassword thiago1 --newpassword="Adm@#87Rtak09" --must-change-at-next-login
    samba-tool user disable thiago1
    samba-tool user enable thiago1
    samba-tool user delete thiago1

Arquivo para criar usuários em massa, users.txt:

    :thiago2:"Adm@#87Rtak09"
    :thiago3:"Adm@#87Rtak09"
    :thiago4:"Adm@#87Rtak09"

Criar usuários:

    apt-get install gawk
    gawk -F ":" '{ print $2,$3 }' users.txt | while read LISTA; do $(echo "/usr/bin/samba-tool user add $LISTA --must-change-at-next-login --use-username-as-cn"); done;


Gerenciando grupo:

    samba-tool group add sti
    samba-tool group add prolauno --description="pc for students"
    samba-tool group list
    samba-tool group addmembers sti thiago1
    samba-tool group addmembers sti proaluno
    samba-tool group listmembers sti
    samba-tool group addmembers sti "thiago2,thiago3"
    samba-tool group removemembers sti thiago3
    samba-tool group delete sti

Arquivo para criar grupos em massai grupos.txt:

    :grupo1:grupo1 descripion
    :grupo2:grupo2 descripion

Criar grupos:

    gawk -F ":" '{ print $2 }' grupos.txt | while read LISTA; do $(echo "/usr/bin/samba-tool group add $LISTA"); done;
